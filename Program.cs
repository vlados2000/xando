﻿using System;

namespace XandO
{
    class Program
    {
        static string[,] gamePoleMap = new string[3, 3];
        static void DefaultValueGame()
        {
            int count = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    count++;
                    gamePoleMap[i, j] = " [ ] ";
                }
            }
        }
        static void Write()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write("|" + gamePoleMap[i, j] + " ");
                }
                Console.WriteLine();
                Console.WriteLine("-------------------------------");
            }
        }
        static string GetFigure()
        {
            string figure = Console.ReadLine();
            if (figure.ToUpper() != "O" && figure.ToUpper() != "X")
            {
                Console.WriteLine("Wrong choice");
                return figure;
            }
            return figure;
        }
        static bool IsEmpty()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gamePoleMap[i, j] != "X" && gamePoleMap[i, j] != "O")
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        static bool GetWinner(string figure)
        {
            int h = 0;
            int v = 0;
            int d = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (gamePoleMap[i, j] == figure)
                    {
                        h++;
                    }
                    if (gamePoleMap[j, i] == figure)
                    {
                        v++;
                    }
                    if (i == j)
                    {
                        if (gamePoleMap[i, j] == figure)
                        {
                            d++;
                        }
                    }
                    if (3 - i == j)
                    {
                        if (gamePoleMap[i, j] == figure)
                        {
                            d++;
                        }
                    }
                }
            }
            if (h == 3 || v == 3 || d == 3)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static void Main(string[] args)
        {
            DefaultValueGame();
            Write();
        }
    }
}
